-- zone de staging

CREATE TABLE IF NOT EXISTS raw_pm10 (
    "nom_dept" VARCHAR(128),
    "nom_com" VARCHAR(128),
    "insee_com" VARCHAR(8),
    "nom_station" VARCHAR(128),
    "code_station" VARCHAR(32),
    "typologie" VARCHAR(32),
    "influence" VARCHAR(32),
    "nom_poll" VARCHAR(32),
    "id_poll_ue" VARCHAR(32),
    "valeur" REAL,
    "unite" VARCHAR(32),
    "metrique" VARCHAR(32),
    "date_debut" TIMESTAMP WITH TIME ZONE,
    "date_fin" TIMESTAMP WITH TIME ZONE,
    "statut_valid" VARCHAR(8),
    "x_wgs84" REAL,
    "y_wgs84" REAL,
    "x_reglementaire" REAL,
    "y_reglementaire" REAL,
    "OBJECTID" INTEGER,
    "longitude" REAL,
    "latitude" REAL
);

CREATE TABLE IF NOT EXISTS raw_no2 (
    "nom_dept" VARCHAR(128),
    "nom_com" VARCHAR(128),
    "insee_com" VARCHAR(8),
    "nom_station" VARCHAR(128),
    "code_station" VARCHAR(32),
    "typologie" VARCHAR(32),
    "influence" VARCHAR(32),
    "nom_poll" VARCHAR(32),
    "id_poll_ue" VARCHAR(32),
    "valeur" REAL,
    "unite" VARCHAR(32),
    "metrique" VARCHAR(32),
    "date_debut" TIMESTAMP WITH TIME ZONE,
    "date_fin" TIMESTAMP WITH TIME ZONE,
    "statut_valid" VARCHAR(8),
    "x_wgs84" REAL,
    "y_wgs84" REAL,
    "x_reglementaire" REAL,
    "y_reglementaire" REAL,
    "OBJECTID" INTEGER,
    "longitude" REAL,
    "latitude" REAL
);

CREATE TABLE IF NOT EXISTS raw_mesures (
    "nom_dept" VARCHAR(128),
    "nom_com" VARCHAR(128),
    "insee_com" VARCHAR(8),
    "nom_station" VARCHAR(128),
    "code_station" VARCHAR(32),
    "typologie" VARCHAR(32),
    "influence" VARCHAR(32),
    "nom_poll" VARCHAR(32),
    "id_poll_ue" VARCHAR(32),
    "valeur" REAL,
    "unite" VARCHAR(32),
    "metrique" VARCHAR(32),
    "date_debut" TIMESTAMP WITH TIME ZONE,
    "date_fin" TIMESTAMP WITH TIME ZONE,
    "statut_valid" VARCHAR(8),
    "x_wgs84" REAL,
    "y_wgs84" REAL,
    "x_reglementaire" REAL,
    "y_reglementaire" REAL,
    "OBJECTID" INTEGER,
    "longitude" REAL,
    "latitude" REAL
);

-- modele analytique

CREATE TABLE IF NOT EXISTS polluants (
    "id" SERIAL,
    "nom" VARCHAR(32),
    "id_ue" VARCHAR(32),
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS departements (
    "id" SERIAL,
    "nom" VARCHAR(128),
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS communes (
    "id" SERIAL,
    "nom" VARCHAR(128),
    "insee" VARCHAR(8),
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS stations (
    "id" SERIAL,
    "nom" VARCHAR(128),
    "code" VARCHAR(32),
    "typologie" VARCHAR(32),
    "influence" VARCHAR(32),
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS mesures (
    "polluant_id" INTEGER,
    "departement_id" INTEGER,
    "commune_id" INTEGER,
    "station_id" INTEGER,
    "valeur" REAL,
    "unite" VARCHAR(32),
    "metrique" VARCHAR(32),
    "date_debut" TIMESTAMP WITH TIME ZONE,
    "date_fin" TIMESTAMP WITH TIME ZONE,
    "statut_valid" VARCHAR(8),
    "x_wgs84" REAL,
    "y_wgs84" REAL,
    "x_reglementaire" REAL,
    "y_reglementaire" REAL,
    "longitude" REAL,
    "latitude" REAL,
    FOREIGN KEY ("polluant_id") REFERENCES polluants ("id"),
    FOREIGN KEY ("departement_id") REFERENCES departements ("id"),
    FOREIGN KEY ("commune_id") REFERENCES communes ("id"),
    FOREIGN KEY ("station_id") REFERENCES stations ("id")
);
