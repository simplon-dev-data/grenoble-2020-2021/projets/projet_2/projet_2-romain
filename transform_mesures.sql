INSERT INTO mesures
SELECT DISTINCT
    polluants.id,
    departements.id,
    communes.id,
    stations.id,
    "valeur",
    "unite",
    "metrique",
    "date_debut",
    "date_fin",
    "statut_valid",
    "x_wgs84",
    "y_wgs84",
    "x_reglementaire",
    "y_reglementaire",
    "longitude",
    "latitude"
FROM raw_mesures
JOIN polluants ON raw_mesures.id_poll_ue = polluants.id_ue
JOIN departements ON raw_mesures.nom_dept = departements.nom
JOIN communes ON raw_mesures.insee_com = communes.insee
JOIN stations ON raw_mesures.code_station = stations.code
WHERE (stations.id, polluants.id, valeur, date_debut, date_fin) NOT IN (
    SELECT station_id, polluant_id, valeur, date_debut, date_fin
    FROM mesures
);
