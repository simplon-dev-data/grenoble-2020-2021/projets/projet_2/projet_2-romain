-- Moyenne des concentrations journalières par département

SELECT
    departements.nom AS "departement",
    CAST(mesures.date_debut AS date) AS "jour",
    avg(mesures.valeur) AS "moyenne"
FROM mesures
JOIN departements ON mesures.departement_id = departements.id
GROUP BY departements.nom, jour
ORDER BY departements.nom ASC, jour ASC;
