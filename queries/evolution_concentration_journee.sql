-- Moyenne de concentration des polluants par heure de la journée

SELECT
    CAST(extract(hour from CAST(mesures.date_debut AS timestamp)) AS integer) AS "heure",
    polluants.nom AS "polluant",
    avg(mesures."valeur") AS "moyenne"
FROM mesures
JOIN polluants ON mesures.polluant_id = polluants.id
GROUP BY heure, polluants.nom
ORDER BY heure, polluants.nom ASC;
