import csv
import json
import sys


csv_col = [
    "nom_dept",
    "nom_com",
    "insee_com",
    "nom_station",
    "code_station",
    "typologie",
    "influence",
    "nom_poll",
    "id_poll_ue",
    "valeur",
    "unite",
    "metrique",
    "date_debut",
    "date_fin",
    "statut_valid",
    "x_wgs84",
    "y_wgs84",
    "x_reglementaire",
    "y_reglementaire",
    "OBJECTID",
    "longitude",
    "latitude",
]

csv_out = csv.DictWriter(sys.stdout, extrasaction="ignore", fieldnames=csv_col)
csv_out.writeheader()

input_lines = "\n".join(sys.stdin.readlines())
data = json.loads(input_lines)

for f in data["features"]:
    csv_line = dict.fromkeys(csv_col)
    csv_line.update(**f["properties"])
    csv_line["longitude"] = f["geometry"]["coordinates"][0]
    csv_line["latitude"] = f["geometry"]["coordinates"][1]
    csv_out.writerow(csv_line)
