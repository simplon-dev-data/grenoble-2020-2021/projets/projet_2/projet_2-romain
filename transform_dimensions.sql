-- remplissage des polluants avec uniquement les nouvelles valeurs uniques
-- note : utilisation de la construction `WHERE col NOT IN (<subquery>)`

INSERT INTO polluants (nom, id_ue)
SELECT DISTINCT nom_poll, id_poll_ue
FROM raw_mesures
WHERE nom_poll NOT IN (SELECT nom FROM polluants);

-- remplissage des departements avec uniquement les nouvelles valeurs uniques
-- note : utilisation de la construction `WHERE NOT EXISTS (<subquery>)`

INSERT INTO departements (nom)
SELECT DISTINCT nom_dept
FROM raw_mesures
WHERE NOT EXISTS (SELECT nom FROM departements WHERE nom = raw_mesures.nom_dept);

-- remplissage des communes avec uniquement les nouvelles valeurs uniques

INSERT INTO communes (nom, insee)
SELECT DISTINCT nom_com, insee_com
FROM raw_mesures
WHERE nom_com NOT IN (SELECT nom FROM communes);

-- remplissage des stations avec uniquement les nouvelles valeurs uniques

INSERT INTO stations (nom, code, typologie, influence)
SELECT DISTINCT nom_station, code_station, typologie, influence
FROM raw_mesures
WHERE nom_station NOT IN (SELECT nom FROM stations);