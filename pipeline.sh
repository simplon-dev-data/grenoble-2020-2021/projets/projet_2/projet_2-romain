#!/bin/bash

set -ex

NOW=$(date -u +'%Y-%m-%dT%H:%M:%SZ')

mkdir -p data

DATASET_PM10_URL=https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson
DATASET_PM10_JSON="data/dataset_pm10_${NOW}.json"
DATASET_PM10_CSV="data/dataset_pm10_${NOW}.csv"
DATASET_PM10_CSV_DOCKER=".docker/data/dataset_pm10.csv"
curl -o ${DATASET_PM10_JSON} ${DATASET_PM10_URL}
python3 parser.py < ${DATASET_PM10_JSON} > ${DATASET_PM10_CSV}
cp ${DATASET_PM10_CSV} ${DATASET_PM10_CSV_DOCKER}

DATASET_NO2_URL=https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson
DATASET_NO2_JSON="data/dataset_no2_${NOW}.json"
DATASET_NO2_CSV="data/dataset_no2_${NOW}.csv"
DATASET_NO2_CSV_DOCKER=".docker/data/dataset_no2.csv"
curl -o ${DATASET_NO2_JSON} ${DATASET_NO2_URL}
python3 parser.py < ${DATASET_NO2_JSON} > ${DATASET_NO2_CSV}
cp ${DATASET_NO2_CSV} ${DATASET_NO2_CSV_DOCKER}

docker-compose exec -T -e PGPASSWORD=test postgres psql -h postgres -U postgres pollution < warehouse.sql
docker-compose exec -T -e PGPASSWORD=test postgres psql -h postgres -U postgres pollution < load_staging.sql
docker-compose exec -T -e PGPASSWORD=test postgres psql -h postgres -U postgres pollution < transform_dimensions.sql
docker-compose exec -T -e PGPASSWORD=test postgres psql -h postgres -U postgres pollution < transform_mesures.sql
