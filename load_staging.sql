COPY raw_pm10
FROM '/data/dataset_pm10.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);

COPY raw_no2
FROM '/data/dataset_no2.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);

INSERT INTO raw_mesures
SELECT DISTINCT *
FROM raw_pm10
WHERE (nom_station, nom_poll, valeur, date_debut, date_fin) NOT IN (
    SELECT nom_station, nom_poll, valeur, date_debut, date_fin
    FROM raw_mesures
);

INSERT INTO raw_mesures
SELECT DISTINCT *
FROM raw_no2
WHERE (nom_station, nom_poll, valeur, date_debut, date_fin) NOT IN (
    SELECT nom_station, nom_poll, valeur, date_debut, date_fin
    FROM raw_mesures
);
