# Projet 2 : Suivi de la pollution atmosphérique

> Exemple formateur Romain C.

Visualiser et analyser la pollution atmosphérique en région Auvergne-Rhône-Alpes.

## Pré-requis

- `docker`
- `docker-compose`
- `pipenv` (ou Python 3.8)

## Installation

Pour installer l'environnement et les dépendances Python nécessaires:

```bash
pipenv install -d
```

Pour lancer PostgreSQL et Metabase:

```bash
docker-compose up -d
```

## Pipeline ETL

Le pipeline est composé de plusieurs étapes :

1. Récupération des données brutes depuis [https://data-atmoaura.opendata.arcgis.com](https://data-atmoaura.opendata.arcgis.com)
2. Parsing et transformation des données brutes GeoJSON en CSV
3. Création de la structure de l'entrepôt de données dans PostgreSQL (si besoin)
4. Chargement des données brutes dans la zone de _staging_ dans PostgreSQL
5. Transformation des _dimensions_ en données propres (remplissage des tables de catégories)
6. Transformation des mesures en données propres

Pour exécuter le pipeline:

```bash
bash pipeline.sh
```

## Exploitation

### Requêtes SQL

Le dossier [`queries`](queries) contient des requêtes d'exploitation des données propres.
Ces requêtes peuvent servir de base à la création de visualisations des métriques dans Metabase.

### Metabase

Les requêtes d'exploitation peuvent être invoquées avec Metabase,
en ouvrant un navigateur à l'adresse [http://localhost:3000](http://localhost:3000).

### Notebooks

Le dossier [`notebooks`](notebooks) contient les notebook Jupyter interactifs avec
des exemples d'exploitation des données depuis la base de données PostgreSQL.
